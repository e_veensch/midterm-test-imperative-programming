//
//  word_search_puzzle_defs.h
//  Midterm Test Imperative Programming
//
//  Created by Erwin van Veenschoten on 17/11/2018.
//  Copyright © 2018 Erwin van Veenschoten. All rights reserved.
//
#include <string>
#ifndef word_search_puzzle_defs_h
#define word_search_puzzle_defs_h

#define NO_COLUMNS  5
#define NO_ROWS     5
#define NO_WORDS    8

using namespace std;

typedef string Word;

char letters [NO_ROWS][NO_COLUMNS] =
{   {'M', 'E', 'A', 'T', 'K'},
    {'Y', 'C', 'O', 'N', 'I'},
    {'O', 'O', 'I', 'L', 'N'},
    {'C', 'R', 'O', 'D', 'D'},
    {'D', 'E', 'P', 'E', 'R'}
};

Word words [NO_WORDS] =
{   "PER",
    "EAT",
    "SLEEP",
    "DRINK",
    "REPEAT",
    "KIND",
    "PRO",
    "WHATEVER"
};

typedef enum
{   Keep,
    Omit
}   Status;

Status status [NO_ROWS][NO_COLUMNS]
{   {Keep, Keep, Keep, Keep, Keep},
    {Keep, Keep, Keep, Keep, Keep},
    {Keep, Keep, Keep, Keep, Keep},
    {Keep, Keep, Keep, Keep, Keep},
    {Keep, Keep, Keep, Keep, Keep},
};

typedef enum
{   Decr = -1,
    Same = 0,
    Incr = 1,
}   Delta;

bool fits_in_puzzle (const Word & word,
                     const int row,
                     const int column,
                     Delta d_row,
                     Delta d_column);

bool matches (char puzzle[NO_ROWS][NO_COLUMNS],
              Word word,
              int row,
              int column,
              Delta d_row,
              Delta d_column);

void omit_letters (Status status[NO_ROWS][NO_COLUMNS],
                   Word word,
                   int row,
                   int column,
                   Delta d_row,
                   Delta d_column);

bool word_matches_at (char puzzle[NO_ROWS][NO_COLUMNS],
                      Word word,
                      int & row,
                      int & column,
                      Delta & d_row,
                      Delta & d_column);

bool search_word (char puzzle[NO_ROWS][NO_COLUMNS],
                  Status status[NO_ROWS][NO_COLUMNS],
                  Word word);

int search_words (char puzzle[NO_ROWS][NO_COLUMNS],
                  Status status[NO_ROWS][NO_COLUMNS],
                  Word words[NO_WORDS]);


#endif /* word_search_puzzle_defs_h */
