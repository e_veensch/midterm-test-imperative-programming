//
//  main.cpp
//  Midterm Test Imperative Programming
//
//  Created by Erwin van Veenschoten on 17/11/2018.
//  Copyright © 2018 Erwin van Veenschoten. All rights reserved.
//

#include <stdio.h>
#include "word_search_puzzle_defs.h"

int main() {
    search_words(letters, status, words);
    if(matches(letters, "pro", 4, 2, Decr, Decr))
        printf("true\n");
    else
        printf("false\n");
    // printf("Number of words found: %d\n", search_words(letters, status, words));
    return 0;
}

bool fits_in_puzzle (const Word & word, // type & identifier is used to avoid copying of class (call by reference)
                     const int _row,
                     const int _column,
                     Delta d_row,
                     Delta d_column)
{   size_t word_length = word.length();
    assert(d_row || d_column);
    
    int row = _row + d_row * (int)word_length;
    int column = _column + d_column * (int)word_length;
    
    return (row <= NO_ROWS && column <= NO_ROWS &&
            row >= 0 && column >= 0);
}

bool matches(char puzzle[NO_ROWS][NO_COLUMNS],
             Word word,
             int _row,
             int _column,
             Delta d_row,
             Delta d_column)
{   if (!fits_in_puzzle(word, _row, _column, d_row, d_column))
        return false;
    
    for (int i = 0; i < word.length(); ++i)
    {
        int row = _row + (int)d_row * i;
        int column = _column + (int)d_column * i;
        
        if(puzzle[row][column] != word[i])
            return false;
    }
    return true;
}


void omit_letters (Status status[NO_ROWS][NO_COLUMNS],
                   Word word,
                   int _row,
                   int _column,
                   Delta d_row,
                   Delta d_column)
{
    for (int i = 0; i < word.length(); ++i)
    {
        int row = _row + d_row * i;
        int column = _column + d_column * i;
        
        status[row][column] = Omit;
    }
}

bool word_matches_at (char puzzle[NO_ROWS][NO_COLUMNS],
                      Word word,
                      int & row,
                      int & column,
                      Delta & d_row,
                      Delta & d_column)
{
    for (int loop_rows = 0; loop_rows < NO_ROWS; ++loop_rows)
    {
        for (int loop_columns =0; loop_columns < NO_COLUMNS; ++loop_columns)
        {
            for (int loop_d_row = Decr; loop_d_row <= Incr; ++loop_d_row)
            {
                for (int loop_d_column = -1; loop_d_column <= 1; ++loop_d_column)
                {
                    if(loop_d_row == 0 && loop_d_column == 0)
                        continue;
                    if (matches(puzzle, word, loop_rows, loop_columns, (Delta)loop_d_row, (Delta)loop_d_column))
                    {
                        row = loop_rows;
                        column = loop_columns;
                        d_row = (Delta)loop_d_row;
                        d_column = (Delta)loop_d_column;
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool search_word (char puzzle[NO_ROWS][NO_COLUMNS],
                  Status status[NO_ROWS][NO_COLUMNS],
                  Word word)
{
    int row = 0, column = 0;
    Delta d_row = Same, d_column = Same;
    if (word_matches_at(puzzle, word, row, column, d_row, d_column))
    {
        omit_letters(status, word, row, column, d_row, d_column);
        return true;
    }
    return false;
}

int search_words (char puzzle[NO_ROWS][NO_COLUMNS],
                  Status status[NO_ROWS][NO_COLUMNS],
                  Word words[NO_WORDS])
{
    int no_words = 0;

    for (int index = 0; index < NO_WORDS; ++index)
        if(search_word(puzzle, status, words[index]))
        {
            printf("%s\n", (char*)&words[index]);
            ++no_words;
        }
    return no_words;
}
